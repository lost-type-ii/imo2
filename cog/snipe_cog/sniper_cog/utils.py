from enum import Enum
from termcolor import colored
from functools import partial

class EVENTTYPE(Enum):
    POSTED  = 0
    DELETED = 1
    EXPIRED = 2
    SNIPED = 3
    DEBUG  = 4

red = partial(colored, color = "red")
green = partial(colored, color = "green")
yellow = partial(colored, color = "yellow")


def event_str(event_type:EVENTTYPE) -> str:
    """
    Returns one of these three strings with color higlighting

    "[POSTED              ]"
    "[      DELETED       ]"
    "[             EXPIRED]"
    "[                    SNIPED]"


    """
    red = partial(colored, color="red")
    green = partial(colored, color="green")
    yellow = partial(colored, color="yellow")
    magenta = partial(colored, color="magenta")

    event_string:str = ""

    if event_type is EVENTTYPE.POSTED:
        p_color = green
        event_string = f'[{p_color("POSTED")}                    ]'

    elif event_type is EVENTTYPE.DELETED:
        p_color = red
        event_string = f'[      {p_color("DELETED")}             ]'

    elif event_type is EVENTTYPE.EXPIRED:
        p_color = yellow
        event_string = f'[             {p_color("EXPIRED")}      ]'

    elif event_type is EVENTTYPE.EXPIRED:
        p_color = yellow
        event_string = f'[             {p_color("EXPIRED")}      ]'

    elif event_type is EVENTTYPE.SNIPED:
        p_color = magenta
        event_string = f'[                    {p_color("SNIPED")}]'

    elif event_type is EVENTTYPE.DEBUG:
        p_color = magenta
        event_string = f'{red(["~DEBUG~"])}'

    return event_string




def print_event_and_message(event_type:EVENTTYPE, message:str = "", application_name = ""):
    print(f'{red(f"[{application_name}]")}{event_str(event_type)}:  {message}')