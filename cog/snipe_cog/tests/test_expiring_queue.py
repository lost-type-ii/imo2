import unittest
from sniper_cog import LazyExpiringQueue


class ExpiringQueueTestCase(unittest.TestCase):
	def test_instantiation(self):
		q = LazyExpiringQueue()
		q = LazyExpiringQueue(timeout = 20)

	def test_add(self):
		q = LazyExpiringQueue(timeout = 20)
		q.add("hello")
		q.add("goodbyle")

	def test_pop(self):
		q = LazyExpiringQueue(timeout = 20)
		q.add("hello")
		v = q.pop()
		assert v == "hello"

	def test_depletion(self):
		q = LazyExpiringQueue(timeout=20)
		q.add("hello")
		_  = q.pop()
		v = q.pop()
		assert  v is None

	def test_empty_pop(self):
		q = LazyExpiringQueue(timeout=20)
		v = q.pop()
		assert v is None

	def test_expiration(self):
		import  time
		q = LazyExpiringQueue(timeout = 0.02)
		q.add("brown")
		time.sleep(0.03)
		v = q.pop()
		assert v is None