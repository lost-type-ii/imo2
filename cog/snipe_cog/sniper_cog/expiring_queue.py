import collections
import threading
from pydantic import BaseModel
import time
from typing import Dict, Union



class QueueEvent(BaseModel):
    time:float
    value:Union[str, Dict]


def empty(*args, **kwargs):
    None

class LazyExpiringQueue:

    def __init__(self, timeout = 1, callback = empty()):
        self.timeout = timeout
        self.events = collections.deque()
        self.callback= callback

    def add(self, item):
        self.__purge_queue()
        self.events.append(QueueEvent(time = time.time(), value = item))

    def __purge_queue(self):
        purge_time= time.time()
        in_time = lambda event: purge_time - event.time <= self.timeout
        not_in_time = lambda event: purge_time - event.time > self.timeout

        events  = collections.deque(filter(in_time,     self.events))
        expired = collections.deque(filter(not_in_time, self.events))

        for event in expired:
            self.callback(event)

        self.events = events

    def pop(self):
        self.__purge_queue()
        is_empty = len(self.events) == 0

        if is_empty:
            return None
        else:
            return self.events.popleft().value

class ExpireQueue:
    """Tracks how many events were added in the preceding time period
    """

    def __init__(self, timeout=1, callback = empty()):
        self.lock=threading.Lock()        
        self.timeout = timeout
        self.events = collections.deque()
        self.callback = callback

    def add(self,item):
        """Add event time
        """
        with self.lock:
            self.events.append(item)
            threading.Timer(self.timeout,self.expire).start()

    def __len__(self):
        """Return number of active events
        """
        with self.lock:
            return len(self.events)

    def expire(self):
        """Remove any expired events
        """
        with self.lock:
            v = self.events.popleft()
            self.callback(v)

    def __str__(self):
        with self.lock:
            return str(self.events)


if __name__ == "__main__":
    import time
    q = LazyExpiringQueue()
    q.add("1")
    print(q.events)

    print(time.time())


    a = collections.deque([1,2,3,4,5,6])
    print(a)
    b = collections.deque(filter(lambda x: x%2 == 0, a))
    print(b)

    c = collections.deque()
    print(c.popleft())