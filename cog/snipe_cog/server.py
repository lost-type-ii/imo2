import yaml
from discord.ext import commands

from sniper_cog import SniperCog


configs = yaml.load(open("configs/bot_settings.yaml","r"), Loader = yaml.Loader)
token   = configs["bot.token"]
prefix  = configs["bot.prefix"]

client = commands.Bot(command_prefix = prefix)
client.add_cog(SniperCog(client))

@client.event
async def on_ready():
	print(f"{client.user} connected!")



if __name__== "__main__":
	client.run(token)

