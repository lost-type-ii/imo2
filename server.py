import yaml
from discord.ext import commands
from cog.snipe_cog import SniperCog

configs = yaml.load(open("settings.yaml","r"), Loader = yaml.Loader)
token   = configs["TOKEN"]
prefix  = configs["PREFIX"]

client = commands.Bot(command_prefix = "asdf ")
client.remove_command('help')

client.add_cog(SniperCog(client))

@client.event
async def on_ready():
    print(f'{client.user} connected!')
    print(client)

@client.command(name = "help")
async def help(context:commands.Context):
    await context.send("https://pbcdn1.podbean.com/imglogo/ep-logo/pbblog3053910/ichy_1_.png")

@client.command(name = "ping")
async def ping(context) -> None:
    await context.send("pong")

if __name__ == "__main__":
    client.run(token)