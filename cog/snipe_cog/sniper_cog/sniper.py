import discord
import discord.ext
import datetime

from discord.ext import commands
from expiringdict import ExpiringDict
from pydantic import BaseModel

from .expiring_queue import ExpireQueue, LazyExpiringQueue
from .utils import EVENTTYPE
from .utils import print_event_and_message

from typing import  Union, Optional
class SniperMessage(BaseModel):
    id:int
    content:str
    avatar:Union[str, None]
    username:str


#   Ideas. Kill count: Would encourage more sniping.
#       Served in: <List of servers where sniped>
#       Should dictionaries have timeouts.


from functools import partial


APP_NAME = "Sniper-Cog"

debug = partial(print_event_and_message, application_name=APP_NAME, event_type = EVENTTYPE.DEBUG)


class SniperCog(commands.Cog):
    def __init__(self, bot,
                 channel_memory_span_in_seconds = 10):
        self.bot = bot

        self.memory_span = channel_memory_span_in_seconds
        self.deleted = ExpiringDict(max_len = 1000,
                                    max_age_seconds = self.memory_span)

    @commands.Cog.listener()
    async def on_ready(self):
        print(":::sniper-cog: online")

    @commands.Cog.listener()
    async def on_message(self, message):
        if "!snipe" in message.content:
            return
        print_event_and_message(application_name=APP_NAME,
                                event_type=EVENTTYPE.POSTED,
                                message=message.content)


    @commands.command(name = "snipe")
    async def on_snipe(self, payload:discord.ext.commands.Context):
        channel_id = payload.message.channel.id
        channel_history = self.deleted.get(channel_id)

        if channel_history is None:
            return

        v = channel_history.pop()

        embed = discord.Embed(  colour=discord.Colour(0x983f34),
                                url="https://discordapp.com",
                                description=v["content"])

        embed.set_author(name=v["username"], url="https://discordapp.com",
                         icon_url=v["avatar"])

        print_event_and_message(application_name=APP_NAME,
                                event_type=EVENTTYPE.SNIPED,
                                message=v)

        await self.bot.get_channel(payload.message.channel.id).send(embed = embed)

    @commands.Cog.listener()
    async def on_message_delete(self,message):
        def print_callback(v):
            print_event_and_message(application_name=APP_NAME,
                                    event_type=EVENTTYPE.EXPIRED,
                                    message=v)

        if message.channel.id not in self.deleted.keys():
            self.deleted[message.channel.id] = LazyExpiringQueue(timeout= self.memory_span, callback= print_callback)



        payload = SniperMessage(  id=message.author.id,
                                  content=message.content,
                                  avatar = str(message.author.avatar_url),
                                  username = message.author.display_name)

        print_event_and_message(application_name=APP_NAME,
                                event_type=EVENTTYPE.DELETED,
                                message=payload)

        self.deleted[message.channel.id].add(payload.dict())


